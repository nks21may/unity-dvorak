﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour {

	public Text VelocityDisplay;

	private static DataManager instance;

	List<float> clicks;
	float init = 0;

	public static DataManager getInstance(){
		return instance;
	}

	// Use this for initialization
	void Start () {
		instance = this;
		init = 0;
		clicks = new List<float> ();
	}

	public void clikeo(){
		if (init == 0) {
			init = Time.timeSinceLevelLoad;
		}
		clicks.Add (Time.timeSinceLevelLoad);
	}

	public void desclickeo(){
		if (clicks.Count > 0)
			clicks.RemoveAt (clicks.Count - 1);
	}

	public void clear(){
		init = 0;
		clicks.Clear ();
	}

	public float calculate(){
		float count = (float)clicks.Count / (Time.timeSinceLevelLoad - init);
		count *= 100f;
		return count;
	}

	void Update() {
		VelocityDisplay.text = getText();
	}

	public string getText(){
		return String.Format ("{0:0.00}", calculate ());
	}
		
	public bool AllGood(){
		return true;
	}
}
