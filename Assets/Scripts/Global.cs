﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour {

	public static string[] ActualLevel = {"L2","R2"};

	//colores

	public static Color FONDO = new Color (188f/255f, 188f/255f, 188f/255f, 0f); //#BCBCBC00
	public static Color PANEL = new Color (124f/255f,124f/255f,124f/255f, 188f/255f); //#7C7C7CBC
	public static Color DESHABILITADO = new Color (1f,0f,0f); //ff0000
	public static Color HABILITADO = new Color (51f/255f, 76f/255f, 1f); //334cff
	public static Color SIGUIENTE = new Color(151f/255f,33f/255f,153f/255f); //972199
	public static Color TEXTO = new Color(1f, 1f, 1f); //FFFFFF

	public const int MAXLENGTHWORD = 6;	
	public const int MINLENGTHWORD = 2;
	public const int LENGTHGAME = 55;
	public const float GOAL_CPM = 100f;

	public static void PauseGame(){
		Time.timeScale = 0.0f;
	}

	public static void ResumeGame(){
		Time.timeScale = 1.0f;
	}

	public static bool ItsPaused(){
		return Mathf.Approximately (Time.timeScale, 0.0f);
	}

}
