﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {
	
	private static LevelManager instance;

	public static LevelManager GetInstance(){
		return instance;
	}

	List<List<Button>> TeclasDelNivel = new List<List<Button>>();
	List<Button> TeclasDelSubNivel = new List<Button>();
	int cur = 0;
	int curAcum = 0;
	public TypingManager typingManager;
	string letters = "";

	// Use this for initialization
	void Start () {
		instance = this;
		foreach (string s in Global.ActualLevel) {
			Button[] aux = GameObject.Find (s).GetComponentsInChildren<Button> ();
			if (s [0] == 'L') {
				Array.Reverse (aux);
			}
				
			TeclasDelNivel.Add (new List<Button> (aux));
		}
		foreach (List<Button> item in TeclasDelNivel) {
			DesmarcarTeclas (item);
		}
		cur = 0;
		curAcum = 0;
		GenNextSubLevel ();
	}
	
	public void GenNextSubLevel(){
		DesmarcarTeclas(TeclasDelSubNivel);
		TeclasDelSubNivel.Clear();
		if (DataManager.getInstance() != null) DataManager.getInstance ().clear ();

		if (curAcum > 4) {
			Debug.Log ("Termino tooo");
			return;
		}

		if(cur > 4){ 
			curAcum++;
			cur = curAcum;
			Debug.Log ("Agregando tecla");
			GenNextSubLevel ();
			return;
		}

		foreach (List<Button> L in TeclasDelNivel) {
			TeclasDelSubNivel.Add (L [cur]);
			for (int i = 0; i < curAcum; i++) {
				TeclasDelSubNivel.Add (L[i]);
			}
		}

		cur++;
		MarcarTeclas (TeclasDelSubNivel);

		letters = "";
		foreach (Button b in TeclasDelSubNivel) {
			letters = letters + Char.ToLower (b.name [0]);
		}
		GenerateText (letters);

	}

	public void ResetSubLevel(){
		if (letters == "")
			throw new InvalidOperationException ();
		GenerateText (letters);
	}

	void DesmarcarTeclas(List<Button> Tecl){
		foreach (Button b in Tecl)
			b.image.color = Global.DESHABILITADO;
		
	}

	void MarcarTeclas(List<Button> Tecl){
		foreach (Button b in Tecl)
			b.image.color = Global.HABILITADO;
	}




	public void GenerateText(string s){
		string result = "";
		System.Random rng = new System.Random ();

		while (result.Length <= Global.LENGTHGAME) {
			result = result + GetRandomWord (s, rng) + " ";	
		}

		result = result.Trim();
		typingManager.LoadText(result);
		typingManager.display.text = result;
	}

	private string GetRandomWord(string s, System.Random rng){
		string res = "";
		int limit = rng.Next(Global.MINLENGTHWORD, Global.MAXLENGTHWORD);
		for (int i = 0; i <= limit; i++) {
			res = res + GetRandomCharacter (s, rng);
		}

		return res;
	}

	private char GetRandomCharacter(string text, System.Random rng){
		int index = rng.Next(text.Length);
		return text[index];
	}
}
