﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TypingManager : MonoBehaviour
{
	public Text display;
	private Button bton;
	public LevelManager levelManager;
	public DataManager dataManager;

	List<Caracter> texton;
	int current = 0;


	void Update(){
		if (Global.ItsPaused ())
			return;
		
		foreach (char c in Input.inputString) {
			if (c.Equals ('\b')) {
				if (current == 0)
					continue;
				current--;
				texton [current].normal ();
				UpdateText ();
				continue;
			}

			if (!c.Equals (texton [current].GetChar ())) {
				texton [current].bad ();
			} else {
				texton [current].good ();
			} 

			if (!(!c.Equals (' ') && texton [current].GetChar ().Equals (' '))) {
				current++;
				dataManager.clikeo ();
			} 

			UpdateText();
			if(current < texton.Count)
				UpdateVisual (c);
			
			CheckEnd ();
		}
    }

	private Boolean CheckEnd(){
		if (texton.Count -1 < current) {
			texton.Clear();
			current = 0;

			UIInfoController.getInstance().Show ();

			return true;
		}
		return false;
	}

	private void UpdateText(){
		string res = "";
		foreach (Caracter c in texton) {
			res = res + c.ToString ();
		}
		display.text = res;
	}

	//get button and presed
	private void UpdateVisual(char c){
		if(bton != null) bton.OnPointerExit (null); //desmarca el boton anterior
		bton = (Button) GameObject.Find (Char.ToUpper(c).ToString()).GetComponent<Button>();
		bton.OnPointerEnter(null);
	}

	private List<Caracter> TextToList(string s){
		List<Caracter> res = new List<Caracter>();
		foreach (char c in s)
			res.Add (new Caracter (c));
		return res;
	}

	public void LoadText(string s){
		texton = TextToList (s);
	}
}

[Serializable]
class Caracter{
	
	int status;

	char c;

	public Caracter(char a){
		c = a;
		status = 0;
	}

	public override string ToString (){
		string hd;
		switch (status) {
		case 1:
			hd = "<color=#00FF00>";
			break;
		case -1:
			hd = "<color=#FF0000>";
			break;
		default:
			hd = "<color=#FFFFFF>";
			break;
		}
		return hd + c.ToString() + "</color>";
	}

	public void bad(){
		status = -1;
	}

	public void normal(){
		status = 0;
	}

	public void good(){
		status = 1;
	}

	public char GetChar(){
		return c;
	}

}