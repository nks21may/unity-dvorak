﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInfoController : MonoBehaviour {

	/*Singleton*/
	private static UIInfoController instance;

	void Start(){
		Hide ();
		instance = this;
	}

	public static UIInfoController getInstance(){
		return instance;
	}
		
	//shows objects with ShowOnPause tag
	public void Show(){
		this.gameObject.SetActive (true);
		float cpm = DataManager.getInstance ().calculate ();

		transform.Find ("ContinueButton")
			.GetChild (0)
			.GetComponent<Text> ()
			.text = (cpm < Global.GOAL_CPM)? "Reintentar":"Siguiente";

		transform.Find ("CPMInfo")
			.GetComponent<Text>()
			.text = "CPM: " + DataManager.getInstance().getText();
		
		Global.PauseGame ();
	}

	//hides objects with ShowOnPause tag
	public void Hide(){
		this.gameObject.SetActive (false);
		Global.ResumeGame ();
	}

	public void InfoButton(Button b){
		if (Global.ItsPaused ()) {
			if (DataManager.getInstance ().calculate() >= Global.GOAL_CPM) {
				Debug.Log ("Aumentando subnivel");
				LevelManager.GetInstance ().GenNextSubLevel ();
			} else {
				Debug.Log ("Reseteando subnivel");
				LevelManager.GetInstance ().ResetSubLevel ();
			}
			Hide ();
		} else {
			Debug.LogError ("Info button. Why this its not paused?");
		}
	}
}
